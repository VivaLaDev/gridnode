package com.vivaladev.comivoyagernode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@SpringBootApplication
public class ComivoyagerNodeApplication {

    private static File graphTask;
    private static FileWriter outputFileWriter;
    private static List<String> pathTask = new ArrayList<>();
    private static Map<Integer, String> resMap = new HashMap<>();


    public static void main(String[] args) {
        SpringApplication.run(ComivoyagerNodeApplication.class, args);
        try {
            init(args);
            String result = process();
            writeAndExit(result);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("OOPS, SOMETHING WENT WRONG, GOODBYE ENAKIN");
            System.exit(1);
        }
    }

    private static void writeAndExit(String result) throws IOException {
        outputFileWriter.write(result);
        outputFileWriter.flush();
        System.out.println("Result is: " + result);
        System.exit(1);
    }

    private static String process() throws IOException {
        Graph graph = readData();
        return getShortestPath(graph);
    }

    private static Graph readData() throws IOException {
        Scanner sc = new Scanner(graphTask);
        StringBuilder graphParameters = new StringBuilder();
        graphParameters.append(sc.nextInt())
                .append(" ");

        while (sc.hasNextInt()) {
            graphParameters.append(sc.nextInt())
                    .append(" ");
        }

        while (sc.hasNext()) {
            pathTask.add(sc.next()
                    .replace("p:", "")
                    .replace("_", " "));
        }


        return Graph.loadGraph(graphParameters.toString());
    }

    private static void init(String[] args) throws IOException {
        Path currentRelativePath = Paths.get("");
        String directory = currentRelativePath.toAbsolutePath().toString();
        System.out.println("Current relative path is: " + directory);
        graphTask = new File(directory + "/" + args[0]);
        if (!graphTask.exists()) {
            System.out.println("File graphX.txt not found from: " + directory + "/" + args[0]);
            System.exit(1);
        }

        File outputFile = new File(directory + "/" + args[1]);
        if (outputFile.exists()) {
            outputFileWriter = new FileWriter(outputFile, false);
        } else {
            System.out.println("File outputX.txt not found from: " + directory + "/" + args[1]);
            System.exit(1);
        }
    }

    private static String getShortestPath(Graph graph) {
        int[][] matrix = graph.getMatrix();
        int res = pathTask
                .stream()
                .map(s -> processSinglePath(matrix, s))
                .min(Integer::compare)
                .orElseThrow(() -> new RuntimeException("error while try to get min result"));
        return formatResult(res, resMap.get(res));
    }

    private static Integer processSinglePath(int[][] matrix, String path) {
        Scanner sc = new Scanner(path);
        List<Integer> integers = new ArrayList<>();
        while (sc.hasNextInt()) {
            integers.add(sc.nextInt());
        }

        int result = 0;
        for (int i = 0; i < integers.size(); i++) {
            if (i + 1 >= integers.size()) {
                break;
            }
            int start = integers.get(i);
            int finish = integers.get(i + 1);
            int value = matrix[start][finish];
            result += value;
        }
        resMap.put(result, path);
        return result;
    }

    private static String formatResult(int res, String path) {
        return "path:" + path.replaceAll(" ", "_") + "\n" + res;
    }
}
