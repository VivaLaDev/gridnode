package com.vivaladev.comivoyagernode;

import lombok.Data;

import java.util.Scanner;

@Data
public class Graph {
    private int count;
    private int[][] matrix;
    private boolean[] marks;

    private Graph(int count) {
        this.count = count;
        matrix = new int[count][count];
        marks = new boolean[count];
    }

    private void setEdge(int a, int b, int weight) {
        matrix[a][b] = weight;
        matrix[b][a] = weight;
    }

    int getEdge(int a, int b) {
        return matrix[a][b];
    }

    boolean hasEdge(int a, int b) {
        return matrix[a - 1][b - 1] != 0;
    }

    public static Graph loadGraph(String params) {
        Scanner sc = new Scanner(params);

        Graph graph = new Graph(sc.nextInt());

        while (sc.hasNext()) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int weight = sc.nextInt();

            graph.setEdge(a - 1, b - 1, weight);
        }

        return graph;
    }

    boolean enter(int pos) {
        if (marks[pos]) {
            return false;
        } else {
            marks[pos] = true;
            return true;
        }
    }

    void leave(int pos) {
        marks[pos] = false;
    }

    int getCount() {
        return count;
    }

    boolean allVisited() {
        for (boolean mark : marks) {
            if (!mark) return false;
        }
        return true;
    }
}